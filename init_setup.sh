#!/bin/bash

DOWNLOAD="$HOME/exa_download"
BASHRC="$HOME/.bashrc"
EXA="$HOME/.local/bin/exa"
VIM="$HOME/.vimrc"

if ! command -v exa &> /dev/null
then
    echo "Installing EXA"
    mkdir "$DOWNLOAD" && cd "$DOWNLOAD" && \
    curl -s https://api.github.com/repos/ogham/exa/releases/latest \
    | grep "browser_download_url.*.zip" \
    | cut -d : -f 2,3 \
    | tr -d \" \
    | wget -qi - && \
    unzip exa-linux-x86_64-v*.zip                                 && \
    mkdir -p "$HOME/.local/bin" && \
    cp -r ./bin/exa "$HOME/.local/bin/exa" && \
    echo "" >> "$BASHRC" && \
    echo "export TERM=\"xterm-256color\"" >> "$BASHRC" && \
    echo "" >> "$BASHRC" && \
    echo 'alias ls="exa -al --color=always --group-directories-first"' >> "$BASHRC" && \
    echo 'alias la="exa -a --color=always --group-directories-first"' >> "$BASHRC" && \
    echo 'alias ll="exa -l --color=always --group-directories-first"' >> "$BASHRC" && \
    echo 'alias lt="exa -aT --color=always --group-directories-first"' >> "$BASHRC" && \
    echo "" >> "$BASHRC" && \
    echo \
    'if [ -d "$HOME/.local/bin" ] ;
      then PATH="$HOME/.local/bin:$PATH" 
    fi' >> "$BASHRC" && \
    rm -rf "$DOWNLOAD"
    echo ""
    echo "run source ~/.bashrc for exa to work"
fi

if ! command -v ranger &> /dev/null
then
    echo ""
    echo "ranger not found, install ranger for better file manager"
fi

if ! command -v nvim &> /dev/null
then
    echo ""
    echo "Neovim not found, install neovim for better vim"
    touch "$VIM"
else
    mkdir -p "$HOME/.config/nvim"
    VIM="$HOME/.config/nvim/init.vim"
    touch "$VIM"
    alias='alias vim="nvim"'
    if ! grep -q "$alias" "$BASHRC"; then
      echo "$alias" >> "$BASHRC"
    fi
fi

# Vim config
# Use spaces instead of tabs
echo ""
echo "writing vim config to $VIM"
# if term supports 256 colors.
if ! grep -q "set t_Co=256" "$VIM"; then
  echo "set t_Co=256" >> "$VIM"
fi
# Use spaces instead of tabs
if ! grep -q "set expandtab" "$VIM"; then
  echo "set expandtab" >> "$VIM"
fi
# Show line number
if ! grep -q "set number" "$VIM"; then
  echo "set number" >> "$VIM"
fi
# Smart Tab
if ! grep -q "set smarttab" "$VIM"; then
  echo "set smarttab" >> "$VIM"
fi
# 4 spaces tab
if ! grep -q "set shiftwidth=4" "$VIM"; then
  echo "set shiftwidth=4" >> "$VIM"
fi
# 4 spaces tab
if ! grep -q "set tabstop=4" "$VIM"; then
  echo "set tabstop=4" >> "$VIM"
fi
# ignore case when searching
if ! grep -q "set ignorecase" "$VIM"; then
  echo "set ignorecase" >> "$VIM"
fi
# nowswapfile
if ! grep -q "set noswapfile" "$VIM"; then
  echo "set noswapfile" >> "$VIM"
fi
# Mouse scrolling
if ! grep -q "set mouse=nicr" "$VIM"; then
  echo "set mouse=nicr" >> "$VIM"
fi
# Mouse selection (Visual Mode)
if ! grep -q "set mouse=a" "$VIM"; then
  echo "set mouse=a" >> "$VIM"
fi
